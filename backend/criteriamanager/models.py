from django.db import models

# Create your models here.

maxLengthForStringField = 100

class Subject(models.Model):
    name = models.CharField(max_length=maxLengthForStringField)
    year = models.PositiveIntegerField()

    def __str__(self):
        return self.name


class Section(models.Model):
    name = models.CharField(max_length=maxLengthForStringField)

    def __str__(self):
        return self.name


class Lesson(models.Model):
    lessonNumber = models.PositiveIntegerField()
    topic = models.CharField(max_length=maxLengthForStringField)
    subject = models.ForeignKey(Subject, on_delete=models.CASCADE)
    section = models.ForeignKey(Section, on_delete=models.CASCADE)

    def __str__(self):
        return self.topic


class Profession(models.Model):
    name = models.CharField(max_length=maxLengthForStringField)

    def __str__(self):
        return self.name


class Qualification(models.Model):
    name = models.CharField(max_length=maxLengthForStringField)
    profession = models.ForeignKey(Profession, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class Subqualification(models.Model):
    name = models.CharField(max_length=maxLengthForStringField)
    qualification = models.ForeignKey(Qualification, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class LearningEffect(models.Model):
    name = models.CharField(max_length=maxLengthForStringField)
    subqualification = models.ForeignKey(Subqualification, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class VerificationCriterion(models.Model):
    name = models.CharField(max_length=maxLengthForStringField)
    learningEffect = models.ForeignKey(LearningEffect, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class Asignment(models.Model):
    lesson = models.ForeignKey(Lesson, on_delete=models.CASCADE)
    verificationCriterion = models.ForeignKey(VerificationCriterion, on_delete=models.CASCADE)


class User(models.Model):
    userName = models.CharField(max_length=20)
    hashedPassword = models.CharField(max_length=maxLengthForStringField)

    def __str__(self):
        return self.userName
