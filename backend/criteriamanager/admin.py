from django.contrib import admin
from .models import *

# Register your models here.
admin.site.register(Subject)
admin.site.register(Section)
admin.site.register(Lesson)
admin.site.register(Profession)
admin.site.register(Qualification)
admin.site.register(Subqualification)
admin.site.register(LearningEffect)
admin.site.register(VerificationCriterion)
admin.site.register(Asignment)
admin.site.register(User)
